package db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.stream.StreamSupport;

public class Gateway {

    private String mongoURL;
    private MongoClient client;
    private MongoDatabase db;
    private MongoCollection collection;
    private Iterable<Document> documents;

    public Gateway(){

    }

    public void init(){
        mongoURL = "mongodb://127.0.0.1:27017";
        client = MongoClients.create(mongoURL);
        db = client.getDatabase("friends");
        collection = db.getCollection("samples_friends");
    }

    public void close(){
        client.close();
    }

    public void getEpisodes(int season) throws DBException{

        documents = collection.find(new Document("season",season));
        long test = StreamSupport.stream(documents.spliterator(), false).count();
        if (test != 0L) {
            for (Document d : documents) {
                System.out.printf("S%.0fE%.0f: %s%n", d.get("season"), d.get("number"), d.get("name"));
            }
        }else{
            close();
            throw new DBException("Type an existent season!");
        }

    }
}
