package application;

import db.DBException;
import db.Gateway;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Type Friends' Season number " +
                "to check its episodes: ");

        try {
            int season = sc.nextInt();
            sc.nextLine();

            Gateway gateway = new Gateway();
            gateway.init();
            gateway.getEpisodes(season);
            gateway.close();
        }
        catch(InputMismatchException e){
            System.out.println("Please type the season number!");
        }
        catch (DBException e){
            System.out.println(e.getMessage());
        }
        sc.close();
    }
}
