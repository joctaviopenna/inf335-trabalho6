# INF 335 Ambientes para Concepção de Software #

## Trabalho 6

- Nome/RG: José Octavio Vitoriano Martines Penna / 40.790.194-2

- Nome/RG: Pablo Gabriel Rodrigues Neves Bedoya / 002.359.913


### Pre-requisitos

1. Com o Jenkins já aberto, criar novo Job selecionando `New Item`
1. Nomear o Job como desejado
1. Selecionar `Pipeline` como tipo do projeto
1. Em `Definition` logo abaixo de `Pipeline` selecionar `Pipeline script from SCM`
1. Configure todos os campos subsequentes como abaixo, e em seguida selecione `Save`:

~~~
    SCM = Git
            
    Repositories:
        Repository URL: https://gitlab.com/joctaviopenna/inf335-trabalho6.git
        
    Credentials = none

    Branches to build:
        Branch Specifier (blank for 'any') = */master

    Repository browser = (Auto)

    Script Path = Jenkinsfile

    Lightweight checkout = true
~~~

### Atividade
Entrega da `trabalho6`

